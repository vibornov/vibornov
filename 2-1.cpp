#include <stdio.h>
#include <stdlib.h>

int main()
{
	int nums[256], summ = 0;
	int i = 0, pol = 0, otr = 0;
	printf("ARRAY: ");
	for (i = 0; i < 256; i++) {	// Fill array (-100..+100)
		nums[i] = rand() % 250 - 50;
		printf("%d; ", nums[i]);
		(nums[i]>0) ? pol++ : otr++;
	}
	printf("\nPositive: %d, Negative: %d\n", pol, otr);
	printf((pol > otr) ? "More Positive" : (pol != otr) ? "More negative" : "They are equal");
	printf("\n");
	return 0;
}