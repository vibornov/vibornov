#include <stdio.h>
#include <stdlib.h>

int main()
{
	int nums[128], sum = 0;
	int i = 0, start = -1, end = -1;
	printf("Array: ");
	for (i = 0; i < 128; i++) {
		nums[i] = rand() % 30 - 15;
		printf("%d; ", nums[i]);
		start = (start == -1 && nums[i] > -1) ? i : start;
	}
	for (i = 127; i >= 0; i--) {
		if (end == -1 && nums[i] > -1) {
			end = i;
			break;
		}
	}
	for (i = start; i <= end; i++)
		sum += nums[i];
	printf("\nTotal: %d\n", sum);
	return 0;
}