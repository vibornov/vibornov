#include <stdio.h>

int main()
{
	char str[256], t;
	int i = 1, curr = 0, prev = 0;
	printf("Enter a string for sort(Alphavit):\n");
	scanf("%s", &str);
	while (str[i]) {
		curr = (str[i] >= 'a' && str[i] <= 'z') ? str[i] - 31 : str[i];
		prev = (str[i - 1] >= 'a' && str[i - 1] <= 'z') ? str[i - 1] - 31 : str[i - 1];
		if (curr < prev) {
			t = str[i];
			str[i] = str[i - 1];
			str[i - 1] = t;
			i -= 2;
		}
		i++;
	};
	printf("%s\n", str);
	return 0;
}