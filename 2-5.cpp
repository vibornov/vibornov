#include <stdio.h>

int main()
{
	int i = 0, d = 0, spaces = 0;
	printf("Enter a diagonal: ");
	scanf("%d", &d);
	if (d > 79) {
		d = 79;
		printf("Diagonal should be less than 80! Diagonal = 79\n\n");
	};
	for (i = 0; i < d / 2; i++) {
		for (int k = 0; k < d; k++)
			printf((k <= d / 2 + i && k >= d / 2 - i) ? "*" : " ");
		printf("\n");
	}
	for (i = d / 2; i < d; i++) {
		for (int k = 0; k < d; k++)
			printf((k <= d / 2 + d - i - 1 && k >= i - d / 2) ? "*" : " ");
		printf("\n");
	}
	return 0;
}