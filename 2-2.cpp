#include <stdio.h>

int main()
{
	char str[256];
	int i = 0;
	bool w = false;
	printf("Enter a string for divide:\n");
	fgets(str, 256, stdin);
	while (str[i]) {
		if (str[i] != 32) {
			printf("%c", str[i]);
			w = true;
		}
		else if (w) {
			w = false;
			printf("\n");
		}
		i++;
	};
	return 0;
}